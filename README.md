# GitLab CI

## Padrão para nomes dos arquivos

Para tornar os nomes dos arquivos mais descritivos, facilitar a busca de pipelines de determinada tecnologia ou serviço e também para evitar que tenhamos arquivos com o mesmo nome (`gitlab-ci.yml`), o seguinte padrão foi definido:

- Utilize a principal tecnologia, o serviço e o cloud provider separados por um "-"

```
react-s3-aws.yml
```

- Utilize uma numeração sequencial para situações em que o nome se repete

```
node-lambda-aws-2.yml
```

## Atenção

Esse repositório é público e arquivos `gitlab-ci.yml` podem conter dados sensíveis (senhas de bancos de dados e chaves do cloud provider), remova essas informações antes de enviar os commits.
